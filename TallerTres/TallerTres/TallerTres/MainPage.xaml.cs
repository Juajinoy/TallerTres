﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TallerTres
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //INICIO DE BOTON A PAGINA REGISTAR PERSONA
        async private void PaginaPersonas(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearPersonas());
        }
        //INICIO DE BOTON A PAGINA REGISTAR PERSONA



        //INICIO DE BOTON A PAGINA REGISTRAR USUARIO
        async private void PasarUsuarios(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearUsuarios());
        }
        //FIN DE BOTON A PAGINA REGISTRAR USUARIO

        

    }
}
