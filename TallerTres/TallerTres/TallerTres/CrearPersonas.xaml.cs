﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerTres.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearPersonas : ContentPage
    {
        public CrearPersonas()
        {
            InitializeComponent();
        }



        // INICIO DE BOTON A PAGINA LISTAR PERSONAS
        async private void PasarListar(object sender, EventArgs e)
        {
            int selectedIndex = Picker.SelectedIndex;

            if (string.IsNullOrEmpty(entryNombre.Text) || string.IsNullOrEmpty(entryTelefono.Text)
                || string.IsNullOrEmpty(entryEmail.Text) || (selectedIndex==-1))
            {

                await DisplayAlert("Error", "Debe escribir los datos completos", "Ok");

            }
            else
            {
                CreateTask();
                entryEmail.Text = "";
                entryNombre.Text = "";
                entryTelefono.Text = "";
                Picker.SelectedIndex = -1;


                await Navigation.PushAsync(new ListarPersonas());
            }
        }
        // FIN DE BOTON A PAGINA LISTAR PERSONAS

        //INICIO DE GUARDAR EN BASE DE DATOS
        public void CreateTask()
        {

            int selectedIndex = Picker.SelectedIndex;
           

            Personas personas = new Personas()
            {
                Nombre = entryNombre.Text,
                Telefono = entryTelefono.Text,
                Email = entryEmail.Text,
                Genero = Picker.Items[selectedIndex]
        };

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Personas>();

                var result = connection.Insert(personas);
                if (result > 0)
                {
                    DisplayAlert("Correcto", "Sus datos se guardaron correctamente", "Ok");
                }
                else
                {
                    DisplayAlert("Error", "Sus datos no fueron guardados", "Ok");
                }
            }

        }
        //FIN DE GUARDAR EN BASE DE DATOS

        //INICION DE METODO DE LISTAR
        async private void PasarAListar()
        {
            await Navigation.PushAsync(new ListarPersonas());
        }
        //FIN DE METODO DE LISTAR


    }
}