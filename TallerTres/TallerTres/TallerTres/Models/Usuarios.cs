﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Usuarios
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id_u { get; set; }

        [MaxLength(150)]
        public string Nombre_usuario { get; set; }
        public string Password { get; set; }
        public string Avatar { get; set; }
        public Boolean Estado { get; set; }
    }
}
