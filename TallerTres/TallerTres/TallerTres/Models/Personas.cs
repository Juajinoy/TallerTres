﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Genero { get; set; }

    }
}
