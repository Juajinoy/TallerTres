﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearUsuarios : ContentPage
	{
		public CrearUsuarios ()
		{
			InitializeComponent ();
		}

        //INICIO DE BOTON A PAGINA LISTAR USUARIOS
        async private void PasarListarUsuarios(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryNameUsu.Text) || string.IsNullOrEmpty(entryPassword.Text)
                || string.IsNullOrEmpty(entryAvatar.Text))
            {
                await DisplayAlert("Error","Debe escribir los datos completos","Ok");
            }
            else
            {
                CreateUser();
                entryNameUsu.Text = "";
                entryPassword.Text = "";
                entryAvatar.Text = "";
                

                await Navigation.PushAsync(new ListarUsuarios());
            }
        }
        //FIN DE BOTON A PAGINA LISTAR USUARIOS

        //INICIO DE BOTO PARA GUARDAR USUARIOS
        public void CreateUser()
        {
            Usuarios usuario = new Usuarios()
            {
                Nombre_usuario = entryNameUsu.Text,
                Password=entryPassword.Text,
                Avatar=entryAvatar.Text,
                Estado=true

            };

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Usuarios>();

                var result = connection.Insert(usuario);
                if (result>0)
                {
                    DisplayAlert("Corrrecto","Sus datos se guardaron correctamente","Ok");
                }
                else
                {
                    DisplayAlert("Error","Sus datos no fueron guardados","Ok");
                }
            }


        }

        //FIN DE BOTO PARA GUARDAR USUARIOS

        //INICIO DE BOTON PASAR A LISTAR
        async private void PasarAListar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarUsuarios());
        }
        //FIN DE BOTON PASAR A LISTAR
    }
}