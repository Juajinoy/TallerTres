﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerTres.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListarUsuarios : ContentPage
	{
		public ListarUsuarios ()
		{
			InitializeComponent ();
		}

        //INICIO DE METODO PARA LISTAR USUARIOS
        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<Usuarios> listaUsuarios;
                listaUsuarios = conecction.Table<Usuarios>().ToList();

                listViewUsuarios.ItemsSource = listaUsuarios;
            }
        }
        //FIN DE METODO PARA LISTAR USUARIOS
    }
}